<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'project11');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<MKOI|!P?1l3CtTd|,y<Z,yoK3n9n/(&_bzo=RF%pcM^uOFzdrWAXLr*v,awScXT');
define('SECURE_AUTH_KEY',  'Kvv3eVHzemPnsR{UPg+qw}/&x+CTB]4D&WDVe6A#8i&#OxQC{MbnZzS0@b8[pZb:');
define('LOGGED_IN_KEY',    'BY6]eoB8Gkoc3?Glu,EX`3u#rv?GQ?mG4Xod*WZ2?t/k|hp!NhMR<1,YVj>^fu{:');
define('NONCE_KEY',        'Fy&pfUy,(gKyJN1l&fAY1elJL-|0ZieY[W%emHfIW|~#d?En#z]BV)]<H+P>Qo~I');
define('AUTH_SALT',        't})6` k#uf*Mntgbn3:&nJcjo6D8&Kd=qiFQ8[%uK$qDt+mWUg7 @7:kY[PrQ|f,');
define('SECURE_AUTH_SALT', 'MO`]2ILv>>&jL{[>=,JLmAKEOfe2+vuYU>y_[y`@{&&W&6qV]=Su!ANxcP%aZuw:');
define('LOGGED_IN_SALT',   'R)(PA&FyG25B(~Gc5<BXxXj`CNia6.A}C9NSVE)fj10eMHK@m.?AD?jYP:aZrE@Z');
define('NONCE_SALT',       ' ty76Fs,Lpdn6T%z`jw:,eL6K&oQb)P<n7gA,y13mD!^,j]55G?mL&DR@MuTNyZ3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
